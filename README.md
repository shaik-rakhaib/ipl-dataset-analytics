
# Installation

 Clone the repository to your local machine:

   ```bash
   git clone https://github.com/your-username/ipl-economical-bowlers.git
   cd ipl-economical-bowlers
   ```
* After cloning the repository, it is recommended to install a virtual environment to run the desired python file.
  
* To install a virtual environment, run this command in your terminal (make sure to run it in same directory as the python files are in)
  ```bash
  python3 -m venv 'env_name'
  ```
* To activate the virtual environment, run this command,
  ```bash
  source 'env_name'/bin/activate
  ```
* If everything has gone well without any issues, your shell prompt(python in this case) should start with the name of your virtual environment and should look something like this.
  ```bash
  (my_env) shaik-rakhaib@shaik-aspire:~
  ```
* In my case , 'my_env' is virtual environment that I activated.

## Requirements

- All the required libraries and packages are mentioned in the requirements.txt file.
  
* You can run this command to install the required libraries.
  ```bash
  pip install -r requirements.txt
  ```
### Now you are ready to run all the python files to visualize the raw data.
