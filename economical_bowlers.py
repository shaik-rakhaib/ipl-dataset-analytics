'''
    plots the top 10 economical bowlers in the year 2015
'''
import csv
import matplotlib.pyplot as plt
def calc_top_10_economical_bowlers():
    '''calculate the top economical bowlers in 2015'''
    matches_2015=[]
    with open('./csv_files/matches.csv','r',encoding='utf-8') as csv_file:
        match_data=csv.DictReader(csv_file)
        for row in match_data:
            if row['season']=='2015':
                matches_2015.append(row['id'])
    with open('./csv_files/deliveries.csv','r',encoding='utf-8') as csv_file:
        match=csv.DictReader(csv_file)
        top_bowlers={}
        for row in match:
            if row['match_id'] in matches_2015:
                if row['bowler'] not in top_bowlers:
                    top_bowlers[row['bowler']]=(int)(row['total_runs'])
                else:
                    top_bowlers[row['bowler']]+=(int)(row['total_runs'])
        for runs in top_bowlers.values():
            runs/=6
        top_n_bowlers=(sorted(top_bowlers.items(),key=lambda item: item[1]))
        top_bowlers=dict(top_n_bowlers[:10])
        return top_bowlers

def plot_top_10_economical_bowlers(bowlers, runs):
    '''plots the top 10 economical bowlers'''
    plt.figure(figsize=(12, 6))
    plt.bar(bowlers, runs, color='dodgerblue')
    plt.xlabel('Bowlers')
    plt.ylabel('Economical Rate')
    plt.title('Top 10 economical bowlers')
    plt.tight_layout()
    plt.show()

def execute():
    '''the execution function'''
    top_10_economical_bowlers=calc_top_10_economical_bowlers()
    bowler=list(top_10_economical_bowlers.keys())
    runs_per_over=list(top_10_economical_bowlers.values())
    plot_top_10_economical_bowlers(bowler,runs_per_over)
execute()
