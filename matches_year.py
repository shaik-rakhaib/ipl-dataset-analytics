'''
    plots the total matches played per year
'''
import csv
import matplotlib.pyplot as plt

def calc_matches_by_year():
    '''calculate matches played per year throughout IPL'''
    with open('./csv_files/matches.csv','r',encoding="utf-8") as csv_file:
        match=csv.DictReader(csv_file)
        year_by_matches={}
        for row in match:
            if row['season'] not in year_by_matches:
                year_by_matches[row['season']]=1
            else:
                year_by_matches[row['season']]+=1
        return year_by_matches  

def plot_matches_by_year(year, no_of_matches):
    '''plots the total matches played per year'''
    plt.figure(figsize=(12, 6))
    plt.bar(year, no_of_matches, color='dodgerblue')
    plt.xlabel('Year')
    plt.ylabel('Number of matches held')
    plt.title('Number of Matches held during every Year')
    plt.xticks(rotation=0)
    plt.tight_layout()
    plt.show()
    
def execute():
    '''the execution function'''
    matches_by_year=calc_matches_by_year()
    year=list(matches_by_year.keys())
    no_of_matches=list(matches_by_year.values())
    plot_matches_by_year(year,no_of_matches)
    
execute()