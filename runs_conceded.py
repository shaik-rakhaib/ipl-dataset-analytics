'''
    to plot the extra runs conceded per teams in the year 2016
'''
import csv
import matplotlib.pyplot as plt


def calc_extra_runs_in_2016():
    '''Calculates all the extra runs conceded per teams in the year 2016'''
    matchid_in_2016 = []
    with open('./csv_files/matches.csv', 'r', encoding='utf-8') as csv_file:
        match_data = csv.DictReader(csv_file)
        for row in match_data:
            if row['season'] == '2016':
                matchid_in_2016.append(row['id'])
    with open('./csv_files/deliveries.csv', 'r', encoding='utf-8') as csv_file:
        runs_data = csv.DictReader(csv_file)
        extra_runs_by_team = {}
        for row in runs_data:
            if row['match_id'] in matchid_in_2016:
                if row['bowling_team'] not in extra_runs_by_team:
                    extra_runs_by_team[row['bowling_team']] = int(
                        row['extra_runs'])
                else:
                    extra_runs_by_team[row['bowling_team']
                                       ] += int(row['extra_runs'])
        return extra_runs_by_team


def plot_extra_runs_per_team_in_2016(team, no_of_extra_runs):
    '''plots the extra runs conceded per teams in the year 2016'''
    plt.figure(figsize=(12, 6))
    plt.bar(team, no_of_extra_runs, color='dodgerblue')
    plt.xlabel('Team')
    plt.ylabel('No. of extra runs conceded')
    plt.title('No. of extra runs conceded per Teams in the year 2016')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()


def execute():
    '''the execution function'''
    extra_runs_per_team = calc_extra_runs_in_2016()
    team = list(extra_runs_per_team.keys())
    no_of_extra_runs = list(extra_runs_per_team.values())
    plot_extra_runs_per_team_in_2016(team, no_of_extra_runs)


execute()
