'''
    Plots the top 10 RCB players with respect to their runs
'''
import csv
import matplotlib.pyplot as plt

def calc_top_rcb_players():
    '''calculates the top 10 RCB players with respect to their runs'''
    with open('./csv_files/deliveries.csv','r', encoding="utf-8") as csv_file:
        match=csv.DictReader(csv_file)
        rcb_players={}
        for row in match:
            if row['batting_team']=='Royal Challengers Bangalore':
                if row['batsman'] not in rcb_players:
                    rcb_players[row['batsman']]=0
                else:
                    rcb_players[row['batsman']]+=int(row['total_runs'])
    top_n_players=(sorted(rcb_players.items(), key=lambda item: item[1],reverse=True))
    top_10_rcb_players=top_n_players[:10]
    top_10_rcb_players=dict(top_10_rcb_players)
    return top_10_rcb_players

def plot_top_rcb_players(players, runs):
    '''plots the top 10 players in RCB with respect to the runs'''
    plt.figure(figsize=(12, 6))
    plt.bar(players, runs, color='dodgerblue')
    plt.xlabel('Players')
    plt.ylabel('Total Runs')
    plt.title('Top 10 RCB players')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()
       
def execute():
    '''the execution function'''
    #Task 1:
    top_rcb_players=calc_top_rcb_players()
    plot_top_rcb_players(list(top_rcb_players.keys()),list(top_rcb_players.values()))
    
execute()