'''
Plots the total runs by every team in the history of IPL
'''
import csv
import matplotlib.pyplot as plt

def plot_total_runs(teams, runs):
    '''plots the total runs by all the teams during their IPL journey'''
    plt.figure(figsize=(12, 6))
    plt.bar(teams, runs, color='dodgerblue')
    plt.xlabel('Team')
    plt.ylabel('Total Runs')
    plt.title('Total Runs Scored by Each Team in IPL History')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()

def calc_total_runs_by_teams():
    '''calculates the total runs by all the teams during their IPL journey'''
    with open('./csv_files/deliveries.csv','r', encoding="utf-8") as csv_file:
        match = csv.DictReader(csv_file)
        total_runs_by_team={}
        for row in match:
            batting_team = row['batting_team']
            total_runs = int(row['total_runs'])
            if batting_team not in total_runs_by_team:
                total_runs_by_team[batting_team]=total_runs
            else:
                total_runs_by_team[batting_team]+=total_runs
        return total_runs_by_team

def execute():
    '''the execution function'''
    total_runs_by_team = calc_total_runs_by_teams()
    teams = list(total_runs_by_team.keys())
    runs = list(total_runs_by_team.values())
    plot_total_runs(teams, runs)

execute()
