'''
    plots the frequency of umpires with regard to their country
'''
import csv
import matplotlib.pyplot as plt

def calc_umpir_freq():
    '''calculates the frequency of umpires with regard to their country'''
    with open('./csv_files/umpires.csv','r',encoding="utf-8") as csv_file:
        umpire_data=csv.DictReader(csv_file)
        umpire_country={}
        for row in umpire_data:
            if row[' country'] !=' India':
                if row[' country'] not in umpire_country:
                    umpire_country[row[' country']]=1
                else:
                    umpire_country[row[' country']]+=1
        return umpire_country

def plot_umpire_freq(country, count):
    '''plots Count of Umpires from different countries in IPL '''
    plt.figure(figsize=(12, 6))
    plt.bar(country, count, color='dodgerblue')
    plt.xlabel('Country')
    plt.ylabel('Frequency')
    plt.title('Count of Umpires from different countries in IPL ')
    plt.xticks(rotation=0)
    plt.tight_layout()
    plt.show()

def execute():
    '''the execution function'''
    umpire_fre_country=calc_umpir_freq()
    country=list(umpire_fre_country.keys())
    count=list(umpire_fre_country.values())
    plot_umpire_freq(country,count)

execute()
