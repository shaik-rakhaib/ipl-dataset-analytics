'''
To plot a stacked bar chart which shows year vs no. of matches played by every team
'''
import csv
import matplotlib.pyplot as plt
def plot_year_matches_team(years,teams,team_counts):
    '''plots the stacked bar'''
    plt.figure(figsize=(12, 6))
    bottom = [0] * len(years)
    for team in teams:
        plt.bar(years, team_counts[team], label=team, bottom=bottom)
        bottom = [bottom[i] + team_counts[team][i] for i in range(len(years))]
    plt.xlabel('Year')
    plt.ylabel('Number of Matches')
    plt.title('Number of Matches played per Team per Year in IPL')
    plt.xticks(years)
    plt.legend(loc='upper right', bbox_to_anchor=(1.15, 1))
    plt.tight_layout()
    plt.show()

def calc_year_matches_team():
    '''transforms the csv file into the data structure required by the library'''
    with open('./csv_files/matches.csv','r',encoding='utf-8') as csv_file:
        ipl_data=csv.DictReader(csv_file)
        matches_per_year={}
        for row in ipl_data:
            if row['team1']=='Rising Pune Supergiant':
                row['team1']='Rising Pune Supergiants'
            if row['team2']=='Rising Pune Supergiant':
                row['team2']='Rising Pune Supergiants'
            if row['season'] not in matches_per_year:
                matches_per_year[row['season']]={}
            if row['team1'] not in matches_per_year[row['season']]:
                matches_per_year[row['season']][row['team1']]=1
            else:
                matches_per_year[row['season']][row['team1']]+=1
            if row['team2'] not in matches_per_year[row['season']]:
                matches_per_year[row['season']][row['team2']]=1
            else:
                matches_per_year[row['season']][row['team2']]+=1
        return matches_per_year

def execute():
    '''execution starts'''
    matches_per_year=calc_year_matches_team()
    years = list(matches_per_year.keys())
    teams = list(set(team for teams in matches_per_year.values() for team in teams.keys()))
    team_counts = {team: [matches_per_year[year].get(team, 0) for year in years] for team in teams}
    plot_year_matches_team(years,teams,team_counts)

execute()
